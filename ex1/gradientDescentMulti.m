function [theta, J_history] = gradientDescentMulti(X, y, theta, alpha, num_iters)
%GRADIENTDESCENTMULTI Performs gradient descent to learn theta
%   theta = GRADIENTDESCENTMULTI(x, y, theta, alpha, num_iters) updates theta by
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m = length(y); % number of training examples
J_history = zeros(num_iters, 1);

for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCostMulti) and gradient here.
    %

    % Explanation: this is a vectorized version of the inner formula for the sum of all i:m:
    % x * theta computes the hypohtesis for all theta in a vector, subtracting y means the deltas are in the resulting vector
    deltas = (X * theta - y);
    
    % To calculate the result, we have to multiply every delta with the corresponding x value (in the equation this is the *x(i,j) in the sum.
    % The result we want is a 2x1 matrix containing the correct value for the theta - meaning we have to transpose X so we have 2xm * mx1 = 2x1
    % Then the result is theta - the previous result multiplied with the learning rate and divided by m
    theta = theta - (alpha*(X' * deltas))/m;






    % ============================================================

    % Save the cost J in every iteration    
    J_history(iter) = computeCostMulti(X, y, theta);

end

end
