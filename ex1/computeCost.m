function J = computeCost(X, y, theta)
%COMPUTECOST Compute cost for linear regression
%   J = COMPUTECOST(X, y, theta) computes the cost of using theta as the
%   parameter for linear regression to fit the data points in X and y

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta
%               You should set J to the cost.

% Explanation: First we compute the inner term (h(xi)-y)^2 - this 
% can be vectorized to X * theta -y which performs the necessary operations 
% for all elements and then using .^2 to square all elements in the resulting
% vector. To calculate the result for all i, we use the sum command to sum all
% entries in the vector and then multiply with the rest of the formula

J = sum((X * theta - y).^2) * 1/(2*length(y))



% =========================================================================

end
