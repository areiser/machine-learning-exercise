function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta

for j = 1:size(theta)
    current_sum = 0;
    for i = 1:m
        
        current_sum += ((sigmoid(X(i,:) * theta) - y(i)) * X(i, j));
    end
    1/m * current_sum;
    grad(j) = 1/m * current_sum ;
end

% Regularize values for gradient
for j = 2:size(theta)
    grad(j) += theta(j)*(lambda/m);
end

cost_sum = 0;
for i = 1:m
    cost_sum += (-y(i)*log(sigmoid(X(i,:) * theta)) - (1 - y(i)) * log(1 - sigmoid(X(i,:) * theta)));
end

regular_theta_sum = 0;
for i = 2:size(theta)
    regular_theta_sum += theta(i)^2;
end
J = 1/m * cost_sum + (lambda/(2*m)) * regular_theta_sum;




% =============================================================

end
